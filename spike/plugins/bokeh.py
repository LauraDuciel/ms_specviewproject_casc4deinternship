#!/usr/bin/env python 
# encoding: utf-8

"""displaying using bokeh

1D only so far

do 

from bokeh.plotting import output_notebook
output_notebook()

if you are running inside a notebook, otherwise, do 

from bokeh.plotting import output_file()
output_file("myplot.html")

"""

from __future__ import print_function
from spike import NPKError
from spike.NPKData import NPKData_plugin, parsezoom
import numpy as np

import bokeh
import bokeh.plotting as bk
from bokeh.models import Range1d

BokehMax = 60000000

########################################################################
def bokeh(npkd, scale = 1.0, absmax = None, show = False, label = None, new_fig = True, axis = None,
                zoom = None, xlabel="_def_", ylabel = "_def_", title = None, figure = None,
                linewidth=1, color = None, plot_width=600, plot_height=400,sizing_mode=None,tools="pan, box_zoom, undo, redo, reset, save"):
    """
    check .display() for doc
    """
    if sizing_mode is not None:
        p = bk.figure(tools=tools,title=title, sizing_mode='scale_width')
    else:
        p = bk.figure(tools=tools,title=title, plot_width=600, plot_height=400)  
    if xlabel == "_def_":
        xlabel = npkd.axis1.currentunit
    if ylabel == "_def_":
        ylabel = "a.u."

    if xlabel is not None:
        p.xaxis.axis_label = xlabel
    if ylabel is not None:
        p.yaxis.axis_label =  ylabel


    if npkd.dim == 1:
        if not absmax:  # absmax is the largest point on spectrum, either given from call, or handled internally
            if not npkd.absmax:     # compute it if absent
                #print "computing absmax...",
                npkd.absmax = np.nanmax( np.abs(npkd.buffer) )
        else:
            npkd.absmax = absmax
        mmin = -npkd.absmax/scale
        mmax = npkd.absmax/scale
        step = npkd.axis1.itype+1

        z1, z2 = parsezoom(npkd,zoom)
        while (z2-z1)//step > BokehMax:
            step += 2
            
        if axis is None:
            ax = npkd.axis1.unit_axis()
        else:
            ax = axis
#        fig.set_xscale(self.axis1.units[self.axis1.currentunit].scale)  # set unit scale (log / linear)
#        if self.axis1.units[self.axis1.currentunit].reverse and new_fig:           # set reverse mode
#            fig.invert_xaxis()

        p.line(ax[z1:z2:step], npkd.buffer[z1:z2:step].clip(mmin,mmax), legend=label, line_width=linewidth, color=color)

        npkd.bokeh_fig = p

        if show:
            bk.show(p)
        return npkd
NPKData_plugin("bokeh", bokeh)

