
var manage_drop = function(){
    /*
    Manage the folder dropped on the Dropzone
    */

    var list_addr = []
    var progmin = 0

    Dropzone.options.dropz = {
          paramName: "file",                                                 // The name that will be used to transfer the file
          maxFilesize: 10000,     // MB
          init: function () {
          // Set up any event handlers
          this.on('complete', function () {
              location.reload();
            });
          },                                              

          sending: function(file, xhr, data){
              if (typeof(file.fullPath) === "undefined") {
               file.fullPath = file.name;
               } 
              data.append("fullPath", file.fullPath);
              $('.dz-preview').remove()                       // remove the Thumbnails
          },
      }; // end Dropzone.options.dropz
}