"""
app/main/routes.py

This file contains all main routes used for the webapp. 
It also contains some python functions defined and used in the different routes.
"""
from __future__ import print_function
from __future__ import division

import os
opj = os.path.join 
opb = os.path.basename
opd = os.path.dirname
import os.path as op

from app import db
from app.main.forms import EditProfileForm, DataConfigForm,ProcessParamForm
from app.models import User
from app.main import bp
from datetime import datetime
from flask import render_template, flash, redirect, url_for, request, g, \
    jsonify, current_app
from flask_login import current_user, login_required
from werkzeug.utils import secure_filename
from flask_dropzone import Dropzone

from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.layouts import gridplot
from bokeh.embed import components,file_html
from bokeh.models import Range1d,HoverTool,CrosshairTool
from bokeh.models.widgets import DataTable,TableColumn
from bokeh.resources import CDN

from xml.dom import minidom
import glob
import shutil

import numpy as np
from numpy.fft import fft, rfft
import mpld3
from mpld3 import plugins
import tables
import scipy
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import pandas as pd
from IPython.display import HTML

from scipy.signal import fftconvolve

import spike
from spike import FTICR
import spike.File.Apex as Apex
import spike.File.Solarix as Solarix
from spike.FTICR import FTICRData
import spike.plugins.Peaks as Peaks
from spike.plugins import bokeh

import array

def default(self, obj):
    """
    Short function to solve a mpld3 error, a support for numpy array jsonification was missing.
    """
    import numpy
    if isinstance(obj, (numpy.int_, numpy.intc, numpy.intp, numpy.int8,
        numpy.int16, numpy.int32, numpy.int64, numpy.uint8,
        numpy.uint16,numpy.uint32, numpy.uint64)):
        return int(obj)
    elif isinstance(obj, (numpy.float_, numpy.float16, numpy.float32, 
        numpy.float64)):
        return float(obj)
    elif isinstance(obj,(numpy.ndarray,)): #### This is the fix
        return obj.tolist()
    return json.JSONEncoder.default(self, obj)
mpld3._display.NumpyEncoder.default = default

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
ALLOWED_EXTENSIONS = ['txt','xy']
BokehMax = 1000000

def create_spectra_fromxy(current_file_name):
    """
    This function returns a bokeh figure from a .xy file.
    current_file_name is the full name (path) of the .xy file.
    returns a html text corresponding to the bokeh figure created. This html text can be embedded in html templates.
    """
    X=[]
    Y=[]
    with open(current_file_name) as f: #The file is opened and read line by line
        lines = f.readlines()
        for line in lines:
            x,y = line.split() #A space is separates the w and y coords
            X.append(float(x)) #X coord is saved in the X[] created before
            Y.append(float(y)) #Same for Y coord.
    resol_prev=len(X) #Trick to know how much the resolution was reduced for the graph to be displayed with a number of points <= BokehMax
    if len(X)>BokehMax: #Case where there was too much points for display
        while len(X)>=BokehMax: #While the number of points is higher than BokehMax, the resolution is divided by two (removing 1 points over 2)
            X_short = []
            Y_short = []
            for i in range(0,len(X),2): #We go through the list by an increment of 2 (Half of the list is not seen and so only half is kept).
                X_short.append(X[i])
                Y_short.append(Y[i])
            X=X_short
            Y=Y_short
        resol_aft=len(X)
        reduc = resol_aft/resol_prev #Check the difference of resolution before and after the reduce for display and print it in the console.
        print('Resolution was reduced by',reduc,' to be shown in browser')
    #Below is the figure configuration for bokeh plotting, p will be displayed through html via the two components div & script.
    p = figure(tools="pan, box_zoom, hover, undo, redo, reset, save", title=opb(current_file_name) + ' spectrum', 
        x_axis_label='m/z', y_axis_label='Intensity', sizing_mode='scale_width')
    p.line(X,Y,color='navy',alpha=0.5)
    html_text=file_html(p,CDN)
    return html_text

def hover(hover_color="#99ccff"):
    return dict(selector="tr:hover",
                props=[("background-color", "%s" % hover_color)])

def create_spectra_fromd(current_file_name):
    """
    This function processes 1D FT-ICR Bruker .d datasets.
    It takes as argument the complete full name of the Bruker .d dataset.
    It returns 4 html texts containing: 
        - the FID
        - the m/z graph obtained from the FID (with SPIKE: apodisation, FT, zero-filling and modulus)
        - A graph with peak picking result (keeping the 1000 most instense peaks  detected)
        - A peak list 
    """
    list_intens=[]
    list_pos=[]
    #The data import is made with spike, a try is done with the solarix process, 
    try:
        f=Solarix.Import_1D(current_file_name)
    #if a file is missing this means that the Apex method needs to be used
    except:
        f = Apex.Import_1D(current_file_name)
    #The bokeh plugin of spike is used to create a bokeh figure that will be displayed
    #First for the FID    
    f.bokeh(xlabel="_def_", ylabel = "Intensity", title = 'FID of: '+opb(current_file_name),
            linewidth=1, color = 'navy' , sizing_mode='scale_width')
    s1 = f.bokeh_fig
    html_text1 = file_html(s1,CDN)
    #Then for the graph itself
    f.unit = 'm/z' #Choosing the right units
    ff = f.copy().apod_sin(maxi = 0.5).zf(zf1=2).rfft().modulus() #A treatment based on spike pipeline is made on a copy of dataset
    f3=ff.copy()
    #Setting the noise parameters
    noise,offset = spike.util.signal_tools.findnoiselevel_offset(ff.get_buffer())
    ff -= offset
    #Performing the peak picking
    ff.pp(threshold=5*noise)
    N = 1001 
    ff.peaks.largest() #Ordering list
    ff.peaks = Peaks.Peak1DList(ff.peaks[:N]) #Only keeping the N-1 most intense peaks picked.
    ff.centroid() #Adjusting the peaks positions.
    for p in ff.peaks:
        p.label = "%.2f"%(ff.axis1.itomz(p.pos)) #Labelling peaks
        list_intens.append(p.intens) #Storing intensities in list
        list_pos.append(ff.axis1.itomz(p.pos)) #Storing positions
    source = ColumnDataSource(data=dict(pos=list_pos,intens=list_intens)) #Create a source useable by Pandas 
    f3.bokeh(xlabel="m/z", ylabel = "Intensity", title = 'Mass Spectrum of: '+opb(current_file_name),
            linewidth=1, color = 'navy', sizing_mode='scale_width') #Here are the settings of the figure s2 that contains the graph. 
    s2 = f3.bokeh_fig #Getting the figure for saving it
    s2.add_tools(HoverTool())
    html_text2=file_html(s2,CDN) #Saving the bokeh html
    fig = f3.bokeh_fig #copying the figure to add peak picking
    HoverTool.tooltips=[("m/z", "$pos")]
    fig.cross(x='pos',y='intens',source=source,color='orange',size=10) #Adding crosses on the graph for each point (position, intensity) from the previous source.
    fig.add_tools(HoverTool())
    #turning the fig containing peak positions to html
    html_text_pp = file_html(fig,CDN) #Keeping the graph with peak picking crosses.
    table_columns = [TableColumn(field='pos', title='m/z'),TableColumn(field='intens', title='Intensity')] #Creation of a Pandas table from the source (position intensities): empty
    peaks_table = DataTable(source=source, columns=table_columns, width=500) #Filled table
    peak_list = file_html(peaks_table,CDN) #Getting the corresponding embeddable html text.
    return html_text1,html_text2, html_text_pp,peak_list


class TopToolbar(plugins.PluginBase):
    """Plugin for moving toolbar of mpld3 to top of figure"""

    JAVASCRIPT = """
    mpld3.register_plugin("toptoolbar", TopToolbar);
    TopToolbar.prototype = Object.create(mpld3.Plugin.prototype);
    TopToolbar.prototype.constructor = TopToolbar;
    function TopToolbar(fig, props){
        mpld3.Plugin.call(this, fig, props);
    };

    TopToolbar.prototype.draw = function(){
      // the toolbar svg doesn't exist
      // yet, so first draw it
      this.fig.toolbar.draw();

      // then change the y position to be
      // at the top of the figure
      this.fig.toolbar.toolbar.attr("y", 3);
      this.fig.toolbar.toolbar.attr("x", 720);

      // then remove the draw function,
      // so that it is not called again
      this.fig.toolbar.draw = function() {}
    }
    """
    def __init__(self):
        self.dict_ = {"type": "toptoolbar"}

def get_contour_data(ax):
    """
    Get informations about contours created by matplotlib.
    ax is the input matplotlob contour ax (cf. fig,ax produced by matplotlib)
    xs and ys are the different contour lines got out of the matplotlib. col is the color corresponding to the lines.
    """
    xs = []
    ys = []
    col = []
    isolevelid = 0
    
    for isolevel in ax.collections:
        isocol = isolevel.get_color()[0]
        thecol = 3 * [None]
        theiso = str(ax.collections[isolevelid].get_array())
        isolevelid += 1
        for i in range(3):
            thecol[i] = int(255 * isocol[i])
        thecol = '#%02x%02x%02x' % (thecol[0], thecol[1], thecol[2])
        
        for path in isolevel.get_paths():
            v = path.vertices
            x = v[:, 0]
            y = v[:, 1]
            xs.append(x.tolist())
            ys.append(y.tolist())
            col.append(thecol)
    return xs, ys, col

def display_2Ddata(fticr2ddata, figname, scale = 1.0, absmax = None, zoom = None, axis = None, xlabel="_def_", ylabel = "_def_", 
    title = None, linewidth=1):
    """
    Create a mpld3 figure from 2D FT ICR Data (Spike)
    fticr2ddata, is the dataset to display, result of importation of .msh5 with spike
    figname, is the name under which the figure will be saved
    scale = 1.0, absmax = None, zoom = None, are specific parameters to select the data to display and the way they are displayed (defined with more details in SPIKE)
    axis = None, xlabel="_def_", ylabel = "_def_", are used to set up the plot
    title is for naming the graph
    returns a html text with the figure that can be embedded into web pages.
    """
    from spike.NPKData import parsezoom
    fticr2ddata.unit ='m/z'
    fig, ax = plt.subplots(figsize=(9,9)) #The figure is created as if it was a classical matplotlib figure
    plt.subplots_adjust(right=0.87,left=0.07,top=0.95,bottom=0.2)
    step2 = fticr2ddata.axis2.itype+1
    step1 = fticr2ddata.axis1.itype+1
    if zoom is None:
        zoom = ((fticr2ddata.axis1.lowmass,fticr2ddata.axis1.highmass),
            (fticr2ddata.axis2.lowmass,fticr2ddata.axis2.highmass))
    z1lo, z1up, z2lo, z2up  = parsezoom(fticr2ddata,zoom)
    if not absmax:  # absmax is the largest point on spectrum, either given from call, or handled internally
        if not fticr2ddata.absmax:     # compute it if absent  - but do it on zoom window ! as this is a killer for large onfile datasets
            fticr2ddata.absmax = np.nanmax( np.abs(fticr2ddata.buffer[z1lo:z1up:step1,z2lo:z2up:step2]) )
    else:
        fticr2ddata.absmax = absmax
    if fticr2ddata.level:
        level = fticr2ddata.level
    else:
        m = fticr2ddata.absmax/scale
        level = (m*0.05, m*0.1, m*0.25, m*0.5)
        if xlabel == "" and ylabel == "":
            ax.set_xticklabels('')
            ax.set_yticklabels('')
    if axis is None:
        axis = (fticr2ddata.axis1.unit_axis(), fticr2ddata.axis2.unit_axis())
    ax.set_yscale(fticr2ddata.axis1.units[fticr2ddata.axis1.currentunit].scale)
    ax.set_xscale(fticr2ddata.axis2.units[fticr2ddata.axis2.currentunit].scale)
    ax.contour(axis[1][z2lo:z2up:step2],
                    axis[0][z1lo:z1up:step1],
                    fticr2ddata.buffer[z1lo:z1up:step1,z2lo:z2up:step2],
                    level)
    if xlabel == "_def_":
        xlabel = fticr2ddata.axis2.currentunit
    if ylabel == "_def_":
        ylabel = fticr2ddata.axis1.currentunit
    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if ylabel is not None:
        ax.set_ylabel(ylabel)
    if title:
        ax.set_title(title)

    xs, ys, col = get_contour_data(ax)
    p = figure(tools="pan, box_zoom, hover, undo, redo, reset, save", title=opb(figname) + ' spectrum', 
        x_axis_label='m/z', y_axis_label='Intensity', sizing_mode='scale_width')
    p.multi_line(xs, ys,
             color=col, line_width=1)
    html_text = file_html(p,CDN)

    return html_text

def define_ext(filename):
    """
    Define extension of a file according to its name.
    filename is the name for which we need the extension (it should include the extension)
    returns extension (for example: txt,csv or html)
    """
    ext = filename.rsplit('.', 1)[1].lower()
    return ext

def check_presence(filename,path):
    """
    This function check if a file is present in a folder.
    filename is the data name
    path is the location where to look for
    returns True or False (Boolean) according to the presence or absence.
    """
    pathlist=os.listdir(path)
    if filename not in pathlist:
        return False
    else:
        return True

def save_html(filename,folderpath,html_text):
    """
    Saves a html_text to a .html file. Using example: for further display.
    """
    name = ".".join([filename.rsplit('.', 1)[0].lower(),'html'])
    full_name="/".join([folderpath,name])
    f = open(full_name,'w')
    f.write(html_text)
    f.close  

def _save_html_bruker(prefix,htmlfid,htmlgraph,htmlpp,peaklist,folderpath):
    """
    Saves the html generated for .d datasets display to .html files which can be used for further display.
    """ 
    descript=['fid','graph','pp','peaklist']
    data=[htmlfid,htmlgraph,htmlpp,peaklist]
    for i in range(len(descript)):
        name = prefix.rsplit('.', 1)[0]+'_'+descript[i]+'.html'
        full_name="/".join([folderpath,name])
        f = open(full_name,'w')
        f.write(data[i])
        f.close

def create_data_html(filename,figures_folder_path,datadir):
    """
    Create a html_text from the dataset containing the display. Calls different functions according to the file extension.
    filename is the name of the dataset to process
    figures_folder_path is the place where to store the figure then (in .html)
    datadir is where the dataset is located
    returns the results of dataset processing with a html format that can be passed to the server.
    """
    html_text=None
    html_text2=None
    html_text_pp=None
    peak_list=None
    ext = define_ext(filename) #Finds the extension of the file concerned
    Allowed_ext = ['xy','msh5','html'] #List of extensions that are supported (except for .d extension that is a specific case)
    figname = ".".join([filename.rsplit('.', 1)[0].lower(),'html']) #Name of the .html figure that will be created and saved
    if ext == 'xy' and not check_presence(figname,figures_folder_path):
        html_text = create_spectra_fromxy("/".join([datadir,filename]))
        save_html(figname,figures_folder_path,html_text)
    elif ext == 'msh5' and not check_presence(figname,figures_folder_path):#2D FT ICR obtained through Spike
        d = FTICR.FTICRData(name=datadir+'/'+filename,mode='onfile',group="resol2") #Import data
        html_text=display_2Ddata(d, scale = 80.0,xlabel="_def_", ylabel = "_def_", 
                                title = '2D FT ICR MS DataSet Display', linewidth=1, figname=figname) #Processing with Spike and Display with mpld3
        save_html(figname,figures_folder_path,html_text)
    elif ext == 'd' and not check_presence(filename.rsplit('.', 1)[0],figures_folder_path):
        fullPath="/".join([figures_folder_path,filename.rsplit('.', 1)[0]])
        os.mkdir(fullPath)
        html_text2, html_text,html_text_pp,peak_list = create_spectra_fromd("/".join([datadir,filename])) #Processing of .d datasets (Spike)
        _save_html_bruker(filename.rsplit('.', 1)[0],html_text,html_text2,html_text_pp,peak_list,fullPath)
    elif ext == 'd' and check_presence(filename.rsplit('.', 1)[0],figures_folder_path): #Load from preexisting figures (4 files to import)
        data = [html_text,html_text2,html_text_pp,peak_list] #Graph, FID, Peakpicking and peak list
        descript=['fid','graph','pp','peaklist']
        fnames=[]
        for i in range(len(descript)): #For each file to import
            name = filename.rsplit('.', 1)[0]+'_'+descript[i]+'.html'
            full_name="/".join([figures_folder_path,filename.rsplit('.', 1)[0],name])
            fnames.append(full_name)
        for i in range(len(fnames)):
            f = open(fnames[i],'r')
            data[i]=f.read()
        html_text, html_text2, html_text_pp,peak_list=data[0],data[1],data[2],data[3]
    elif ext in Allowed_ext and check_presence(figname,figures_folder_path): #Loading dataset already processed if they exist (except for .d datasets)
        full_name = "/".join([figures_folder_path,figname]) #Only 1 file to import
        f = open(full_name,'r')
        html_text=f.read()
    else:
        flash(('This file extension is not - already - supported. Please chose another file to visualise.')) 
    return html_text, html_text2, html_text_pp,peak_list
        
def make_tree(path):
    """
    This function allows to get an object containing the "tree" of folders and files in the upload/user folder.
    path is the  directory from which the "tree" must be done
    Returns tree which is a list of disctionnaries (json).
    """
    tree = [] #Creation of the tree list
    try: lst = os.listdir(path) #Listing of files and folder contains in the directory
    except OSError:
        pass
    else:
        for name in lst: #For each element found previously in lst
            fn = os.path.join(path,name) #The directory to this element is written
            if os.path.isdir(fn): #If fn is a folder then
                sub_dict=dict(id=name,label=name,children=[],selectable=True) #A subdirectory is created and a dictionary containing all elements (in children=[]) of this sub directory is built.
                sub_dict['children']=make_tree(fn) #If an element of the sub folder is also a folder, the maketree function is re-applied.
                tree.append(sub_dict) #The dictionary is added to the tree list
            else:
                sub_dict=dict(id=name,label=name,selectable=True) #If the element of lst is a simple file then a dictionary with its specificities
                tree.append(sub_dict)#is added to the tree list
    return tree #tree is thus a list of dictionnaries that can themselves contain lists (children[])

@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()

@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
def index():
    return render_template('index.html', title='Home')

@bp.route('/mydata', methods=['GET','POST'])
@login_required
def mydata(): #Main function used within the web app
    Dataconfigform = DataConfigForm()#Generate the form (from app/main/forms.py) that is passed to the jinja in mydata.html
    Processparamform = ProcessParamForm()
    target = "/".join([APP_ROOT,'upload',current_user.username]) #Defines the location for file uploading.
    if not os.path.isdir(target): #If the upload folder for the authenticated user does not exist   
        os.mkdir(target) #The folder is created
    uploaded_files = os.listdir(target) #List all files already uploaded in the folder.
    figures_dir = "/".join([APP_ROOT,'upload',current_user.username,'MyFigures'])
    if not os.path.isdir(figures_dir):
        os.mkdir(figures_dir)
    created_figures = os.listdir(figures_dir)
    if request.method == 'POST': #If an upload is request through the dropzone.
        for f in request.files.getlist('file'): #For each file in the list that has been dropped in the dropzone
            file_in_folder_path = request.form.get('fullPath') #Get file from path a js method (manage_drop.js)
            full_path = os.path.join(target, file_in_folder_path) 
            try:
                f.save(full_path)# Save locally the file in the folder upload
            except IOError as e:
                # try creating parent directories
                os.makedirs(opd(full_path))# Makes folder
                f.save(full_path)# Save locally the file in the folder upload
    current_file_name = request.args.get("file_name") #Get back the selected dataset name
    Dataconfigform.File.data=current_file_name#Set by default the file name to the selected displayed file

    if current_file_name: #If a dataset is selected, the extension is checked to make sure it is a supported one.
        html_text,html_text2,html_text_pp,peak_list = create_data_html(current_file_name,figures_dir,target) #Whatever the kind of extension, the same function is called
        if html_text2 is None: #In some cases, not 4 graphs are generated but only 1 (no needs for 4 graphs if not Bruker .d dataset)
            return render_template('mydata.html',Processparam_form=Processparamform, Dataconfig_form=Dataconfigform, 
                html_text=html_text, uploaded_files= uploaded_files, tree=make_tree(target), 
                current_file_name= current_file_name, title='View Your Data')
        else: #If html_text2 is not None, it is the case where we have 4 results to display -.d Bruker datasets)
            return render_template('mydata.html',Processparam_form=Processparamform, Dataconfig_form=Dataconfigform,
                html_text2=html_text2, html_text=html_text,html_text_pp=html_text_pp,peak_list=peak_list, uploaded_files= uploaded_files, tree=make_tree(target), 
                current_file_name= current_file_name, title='View Your Data')
    else: #Case where nothing has been submitted for display.
        return render_template('mydata.html',Processparam_form=Processparamform, Dataconfig_form=Dataconfigform,
            uploaded_files= uploaded_files, tree=make_tree(target), title='View Your Data')

@bp.route('/deletemydata', methods=['GET','POST'])
@login_required
def deletemydata():
    target = "/".join([APP_ROOT,'upload',current_user.username]) #The folder where to search for the file name to delete
    current_file = request.args.get("file_to_delete_name") #File name of selected file to delete is get
    if current_file:
        print(current_file + " was remove from the following directory: " + target)
        try:
            os.remove(opj(target, current_file))
            flash(('The file '+current_file+' was successfully deleted.'))
        except:
            shutil.rmtree(opj(target, current_file))
            flash(('The file '+current_file+' was successfully deleted.'))
    return redirect(url_for('main.mydata'))

@bp.route('/help')
def help():
    return render_template('help.html', title='Help')

@bp.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    page = request.args.get('page', 1, type=int)
    return render_template('user.html', user=user)

@bp.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash(('Your changes have been saved.'))
        return redirect(url_for('main.edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title=('Edit Profile'),
                           form=form)