from flask import request
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, FloatField, RadioField, IntegerField
from wtforms.validators import ValidationError, DataRequired, Length
from app.models import User


class EditProfileForm(FlaskForm): #Flask forms to edit the profile and to check that the chosen username is not already used by someone else
    username = StringField(('Username'), validators=[DataRequired()])
    about_me = TextAreaField(('About me'),
                             validators=[Length(min=0, max=140)])
    submit = SubmitField(('Submit'))

    def __init__(self, original_username, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.original_username = original_username

    def validate_username(self, username):
        if username.data != self.original_username:
            user = User.query.filter_by(username=self.username.data).first()
            if user is not None:
                raise ValidationError(('Please use a different username.'))

class DataConfigForm(FlaskForm):
    File = StringField(('File to process'))
    highmass = FloatField(('Highmass'))
    overwrite = RadioField('Overwrite?',choices=[(True,'Yes'),(False,'No')])
    submit_dataconfig = SubmitField(('Submit Data Configuration'))

    def __init__(self, *args, **kwargs):
        super(DataConfigForm, self).__init__(*args, **kwargs)

class ProcessParamForm(FlaskForm):
    urQRd = RadioField('Do urQRd processing?', choices=[(True,'Yes'),(False,'No')] )
    urank = IntegerField('Choose urQRd rank (Integer ‎∈ [4,100])')
    submit_processparam = SubmitField(('Submit Processing Parameters'))

    def __init__(self, *args, **kwargs):
        super(ProcessParamForm, self).__init__(*args, **kwargs)