from flask import Blueprint

bp = Blueprint('auth', __name__)#Adding a Blueprint for all authentication things called 'auth'

from app.auth import routes
