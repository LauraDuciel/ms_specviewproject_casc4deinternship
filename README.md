# README #

The **MS-SpecView** WebApp is an application aiming to display and in future releases process FT-spectroscopy data. It is developed during an internship within Casc4de (www.casc4de.eu) for the EU_FT-ICR_MS infrastructure project.


![alternativetext](app/static/img/Home_screenshot.png)


## How to use MS-SpecView ? ##

For now, the web app is only a viewer of FT-ICR MS Data in formats .xy ,.d (Bruker) and .msh5 a 2D FT-ICR Data format obtained with the Spike library (https://bitbucket.org/delsuc/spike/src/default/). You also have the Home page giving you information about Casc4de and the EU_FT-ICR_MS Infrastructure and a Profile page to edit your info. 

You have access to an upload of your data from the tab 'MyData' in the navbar. A folder will be created in app/main/upload/username after the creation of your user account.

Then, to visualise,  choose the dataset - just click it - and click on the display button. 

The available display are first graph from .d Bruker file and their FID.


![alternativetext](app/static/img/Graph.png)


![alternativetext](app/static/img/FID.png)

In addition a peak picking is realized with default parameters.


![alternativetext](app/static/img/Peak_picking.png)

and if you have 2D FT-ICR Datasets, they are supported for visualization.


![alternativetext](app/static/img/2D.png)

###Requirements###
To make it run you need to have installed on your computer (or in a virtual environment) all packages listed in the requirements.txt file, for this run:

        pip install -r requirements.txt

and install in addition:

        python3 & Numpy (Anaconda)
        Bootstrap


Then set the environment variable:
For Ubuntu, in the terminal go to the top of the app folder and execute: 

        export FLASK_APP=MS-SpecView.py
If you are using Microsoft Windows run:

        set FLASK_APP=MS-SpecView.py

Finally run:

        flask run


### Author ###
Current Active author for MS-SpecView WebApp is:

* Laura Duciel

        Master2 & Biotech Engineer Student (ESBS) - Intern at Casc4de
        contact: ld@casc4de.eu
